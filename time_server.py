#!/bin/python3 python
import socket
import time

BUFF_SIZE = 1024

time_IP = socket.gethostbyname(socket.gethostname())
time_PORT = 10000

uniSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
while True:
	try:
		uniSock.bind((time_IP, time_PORT))
		break
	except:
		time_PORT = time_PORT + 1

print('Moje IP: {}'.format(time_IP))
print('Moj PORT: {}'.format(time_PORT))

while True:
    data, addr_from = uniSock.recvfrom(BUFF_SIZE)  # otrzymuję wiadomość

    if data == b'time':
        print(data.decode('utf-8'))
        t = time.localtime()
        t = time.strftime("%H:%M:%S", t)
        print('Przesylam czas: ' + t)
        uniSock.sendto( bytes(t, 'utf-8'), addr_from )
    if data == b'exit':
        uniSock.sendto( bytes(t, 'utf-8'), addr_from )
        break
