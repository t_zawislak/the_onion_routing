#!/usr/bin/python3 python

import socket
import struct
from threading import Thread
import func
import sys
import pickle
import time

# Utwórz nowe połączenie udp unicast
UDP_IP = socket.gethostbyname(socket.gethostname())
UDP_PORT = 5000
uniSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
uniSock.settimeout(3)


while True:
    try:
        uniSock.bind((UDP_IP, UDP_PORT))
        break
    except:         # jeśli port jest zajęty, zwiększ numer portu
        UDP_PORT+=1


print("Mój address IP: ", UDP_IP)
print("Numer portu: ", UDP_PORT)
# wczytuję adres "initial node", od którego dostaję listę pozostałych nodów w sieci
initialIP=''
initialPort=0
try:
    with open("./init.txt") as masterIpFile:
        lines = [line.rstrip() for line in masterIpFile]
        for line in lines:
            try:
                socket.inet_aton(line.split()[0])
                initialIP=line.split()[0]
            except:
                print("Format adresu IP w pliku init.txt jest niepoprawny")
                sys.exit(1)
            try:
                initialPort=int(line.split()[1])
            except:
                print("Format portu w pliku init.txt jest niepoprawny")
                sys.exit(1)


            if (initialIP, initialPort) != uniSock.getsockname():
                func.inodes.append( (initialIP, initialPort) )
except:
    print("Nieudana próba odczytu pliku init.txt. Czy plik istnieje?")
    sys.exit(1)

func.GetInitialInodesList(uniSock)
time.sleep(0.5)
print("Początkowa lista serwerów pośredniczących:\n", func.inodes)
# Rozpoczynam pracę serwera pośredniczącego
listen_check = Thread( target=func.check_listen_and_replay, args=(uniSock,))
listen_check.start()
# obsługa poleceń z wiersza poleceń:
isexit = input()
while(isexit != "exit"):
    isexit = input()
print("success", isexit)
func.terminator = True #zakończ wątki
time.sleep(1)
sys.exit(0)
