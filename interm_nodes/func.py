#!/usr/bin/python3 python

import time
import socket
import struct
import sys
import random
import math
import pickle
from threading import Thread



###########################
#   USTAWIENIA  GLOBALNE  #
###########################

BUFF_SIZE = 1024
inodes = []     # tu przechowywane będą adresy i porty wszystkich serwerów pośredniczących
pending = []    # tu przechowywane będą otwarte kanały
terminator = False; # przerywa działanie wątków i zamyka program


###################################################################
#   ODPOWIADA INNYM INODES, POTWIERDZAJĄC SWOJĄ GOTOWOŚĆ W SIECI  #
###################################################################

def check_listen_and_replay(uniSock):
    #   zbiera dane, następnie, z data wydziela adres i payload, po czym wysyła na adres payload
    while True:
    #  JEŚLI UŻYTKOWNIK CHCE ZAKOŃCZYĆ DZIAŁANIE PROGRAMU:
        if terminator :             # jeśli użytkownik wpisał exit to zakończ działanie
            for i in inodes:        # i powiedom wszystkich o końcu pracy
                uniSock.sendto(b"RMV", i)
            break       # kończy działanie pętli i w kosekwencji całej funkcji
    # CZEKAM NA NOWĄ WIADOMOŚĆ
        try:
            data, addr_from = uniSock.recvfrom(BUFF_SIZE)  # otrzymuję zadanie
        except socket.timeout:
            continue
        print ("\nAktualna lista inodes:\n", inodes)
        print ("otwarte kanały:\n ", pending)
    # KWERENDY DO OBSŁUGI LISTY INODES
        # zwraca listę inodes
        if data == b"GET":
            uniSock.sendto( pickle.dumps(inodes) , addr_from)
            continue
        # dodaje adres do inodes
        if data == b"ADD":
            if addr_from not in inodes:
                print("Nowy node: ", addr_from)
                inodes.append(addr_from)
            continue
        # usuwa adres z inodes
        if data == b"RMV":
            if addr_from in inodes:
                print("Usuwam node: ", addr_from)
                inodes.remove(addr_from)
            continue
    # OBSŁUGA *ODPOWIEDZI* OD SERWERA ZEWNĘTRZNEGO / KLIENTA
        # sprawdzam, czy jest to opdowiedź od serwera zewnętrznego
        # obsługuje odpowiedzi tylko od serwerów zewnętrznych
        czy_odpowiedz = False
        if addr_from not in inodes:         # komunikacja zewnętrzna (nie pomiędzy serwerami pośredniczącymi)
            for channel in pending :        #
                if channel[2]==addr_from :  # sprawdzam czy to jest odpowiedź od zewnętrznego klienta
                    print ("Mam odpowiedz od KLIENTA w kanale: ", channel[1])
                    print("Propaguję: ", data + (channel[1]).to_bytes(2, byteorder='big'), " do ", channel[0])
                    uniSock.sendto(  data + (channel[1]).to_bytes(2, byteorder='big'), channel[0])                         # wyślij odpowiedź
                    print("usunąłem kanał o id:", channel[1])
                    pending.remove(channel)
                    czy_odpowiedz = True
                    continue
        if czy_odpowiedz == True:
            continue

    # jeśli nie jest to odpowiedź od klienta zewnętrznego:
        print("OTRZYMAŁEM: ", data)
        # rozpakuj cID i sprawdz czy to nie jest przypadkiem odpowiedz w jednym z kanałów
        cID = int.from_bytes(data[-2:],  byteorder='big');
        # obsługuje odpowiedzi pomiędzy serwerami pośredniczącymi + pomiędzy serwerem pośredniczącym a klientem, który oczekuje na odpowiedź
        for channel in pending :
            if channel[1] == cID:          # jeśli istniał już taki kanał, prześlij mu payload+cID i usuń go z pending[]
                print ("Mam odpowiedz w kanale: ", cID)
                payload = data[:-2].decode()
                name_len = len(str(channel[0])).to_bytes(2, byteorder='big')
            # sprawdzam, czy jest to odpowiedz dla klienta zewnętrznego
                if channel[0] not in inodes:
                    uniSock.sendto( payload.encode(), channel[0])
                else:
                    uniSock.sendto( name_len + str(channel[0]).encode() + payload.encode() + (cID).to_bytes(2, byteorder='big'), channel[0])
                pending.remove(channel)
                print("usunąłem kanał o id:", cID)
                break
        else:   # jeśli nie znalazł cID w liscie otwartych kanałów:
        # to nie odpowiedź, zatem odczytuję adres do dalszej propagacji
            addr_size = int.from_bytes(data[:2], byteorder='big')
            addr_to = eval(data[2:addr_size+2])
            payload = data[addr_size+2:-2].decode()
        # Jeśli to ja jestem odbiorcą końcowym:         # to nie powinno być obiektem zainteresowania sieci, ale na wszelki wypadek dodałem
            if addr_to == uniSock.getsockname():
                print ("nowa wiadomość: ", payload)
                return
        # CZĘŚĆ ODPOWIEDZIALNA ZA PROPAGACJĘ INFORMACJI W SIECI
            pending.append( [addr_from, cID, addr_to] ) # dodaję do listy otwartych kanałów
            print( "Dodaję kanał o id: ", cID, " dla ", addr_from)
        # JEŚLI ADRES DO PROPAGACJI NIE JEST W inodes[] TO PRAWDOPODOBNIE JESTEM WĘZŁEM KOŃCOWYM
        # NIE WYSYŁAM MU ZATEM NUMERU cID
            if addr_to not in inodes:
                print("Propaguję: ", payload.encode(), " do ", addr_to)
                uniSock.sendto(  payload.encode(), addr_to)
            else:
        # JEŚLI NIE JESTEM WĘZŁEM KOŃCOWYM, TO PROPAFUJĘ WIADOMOŚĆ DALEJ.
                print("Propaguję: ", payload.encode() + (cID).to_bytes(2, byteorder='big'), " do ", addr_to)
                uniSock.sendto(  payload.encode() + (cID).to_bytes(2, byteorder='big'), addr_to)                         # pr`opaguj payload dalej



###################################
#  USTALA POCZĄTKOWĄ LISTĘ INODES #
###################################

def GetInitialInodesList(uniSock):
    for i in inodes:   # wczytane z pliku
        try:
            uniSock.sendto(b"GET", i)
            data = uniSock.recv(BUFF_SIZE)
            part = pickle.loads(data)   # rozpakowuje listę inodes
            for j in part:
                if j not in inodes:
                    inodes.append(j)
            if uniSock.getsockname() in inodes:
                inodes.remove( uniSock.getsockname() )
            break
        except socket.timeout:
            inodes.remove(i)    # nie udało się z nim połączyć więc go usuwam
            print("Połączenie z serwerem ", i," z listy nieudane. Próbuję dalej.")
    # powiadom wszystkie inody ze swojej listy, że masz ich adresy
    for i in inodes:
        uniSock.sendto(b"ADD", i)
