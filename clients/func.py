#!/usr/bin/python3 python

import time
import socket
import struct
import sys
import random
import math
import pickle
from threading import Thread

#####################################
#   USTAWIENIA MULTICAST I UNICAST  #
#####################################
BUFF_SIZE = 1024
inodes = [] # lista adrsów serwerów pośredniczących
pending = [] # lista adresów od których oczekuję odpowiedzi
terminator = False; # przerywa działanie wątków i zamyka program


###################################
#  USTALA POCZĄTKOWĄ LISTĘ INODES #
###################################
def GetCurrentList(uniSock, node):
    try:
        uniSock.sendto(b"GET", node)
        data = uniSock.recv(BUFF_SIZE)
        part = pickle.loads(data)
        del inodes[:]
        for j in part:
            inodes.append(j)
        inodes.append(node)
        return True
    except socket.timeout:
        inodes.remove(node)    # nie udało się z nim połączyć więc go usuwam
        print("Połączenie z serwerem ", node, " nieudane. Próbuję dalej.")
    return False # zadziała gdy nie uda się połączyć

def GetInitialInodesList(uniSock):
    for i in inodes:   # wczytane z pliku
        if GetCurrentList(uniSock, i):
            break   # przerywam po pierwszym udanym nawiązaniu kontaktu

def CzytajAdress(uniSock):
    InitialIP=''
    InitialPort=0
    try:
        with open("init.txt") as masterIpFile:
            lines = [line.rstrip() for line in masterIpFile]
            for line in lines:
                try:
                    socket.inet_aton(line.split()[0])
                    InitialIP=line.split()[0]
                except:
                    print("Format adresu IP w pliku init.txt jest niepoprawny")
                    sys.exit(1)
                try:
                    InitialPort=int(line.split()[1])
                except:
                    print("Format portu w pliku init.txt jest niepoprawny")
                    sys.exit(1)
                if (InitialIP, InitialPort) != uniSock.getsockname():
                    inodes.append( (InitialIP, InitialPort) )
    except:
        print("Nieudana próba odczytu adresu z pliku init.txt. Czy plik istnieje?")
        sys.exit(1)
######################################################
#   WYSYŁA PAYLOAD PO ZDEFINIOWANEJ, LOSOWEJ TRASIE  #
######################################################

def send(uniSock, payload, _destination):
    addr_size = sys.getsizeof( str(uniSock.getsockname()).encode() )    # określa rozmiaru adresu
    Npath = 3   # liczba serwerów pośredniczących
    payload_size = sys.getsizeof(payload)
    print("\nmasa wiadomości: ", payload_size )

    if  payload_size > BUFF_SIZE - (Npath+1)*(addr_size+2):   # zakładam, że zawsze wiadomość ma przejsc przez Npath serwerów pośredniczących
        print("Wiadomość jest zbyt długa, proszę wysłać krótszą wiadomość.")           #  + adres i rozmiar odbiorcy końcowego
        return

    if len(inodes) < Npath:
        print("Jest zbyt mało węzłow w sieci. Nie wysyłam wiadomości")
        return
# TWORZĘ ŚCIEŻKĘ, PO KTÓREJ PROPAGOWANA MA BYĆ WIADOMOŚĆ
    path = random.sample( range( len(inodes)), Npath)       # losuj bez powtórzeń ścieżkę (indeksy do inodes[])
    strpath = b""
    for i in path[1:]:          # nie uwzględniam inode[ path[0] ] w wiadomości, bo do niego wysyłam całą wiadomość
        strpath += len( str(inodes[i]) ).to_bytes(2, byteorder='big')
        strpath += str(inodes[i]).encode()
    print("pierwszy inode: ", inodes[path[0]])
    # dodaję na sam koniec rozmiar i adres odbiorcy końcowego
    strpath += len( str(_destination) ).to_bytes(2, byteorder='big')
    strpath += str(_destination).encode()

    print("Wylosowana śceżka: ", strpath)          # bez pierwszego inode
# wylosuj circuit_id, dzięki któremu możliwe będą odpowiedzi od serwera
    cID = (random.randint(1000, 9999)).to_bytes(2, byteorder='big')
# tu byłby dobry mmoment na zaszyfrowanie wiadomości

    print("Wysyłam: ", strpath + payload.encode() + cID, " do ", inodes[path[0]] )
# wyślij wiadomość
    uniSock.sendto( strpath + payload.encode() + cID,    inodes[path[0]]);
    pending.append(inodes[path[0]])   # dodaję do listy, żebym wiedział, że oczekuję odpowiedzi
##############################################
#   NASŁUCHUJE NOWYCH WIADOMOSCI NA UNICAST  #
##############################################

def listen(uniSock):
    while True:
        if terminator :
            break       # kończy działanie
        try:
            data, addr_from = uniSock.recvfrom(BUFF_SIZE);
        except socket.timeout:
            # zaktualizuj
            if len(inodes) != 0:
                GetCurrentList(uniSock, inodes[random.randint(0, len(inodes)-1)] )
            else:
                CzytajAdress(uniSock)
                GetInitialInodesList(uniSock)
            continue
        print("inodes: ", inodes)
        print("OTRZYMAŁEM: ", data, " OD: ", addr_from)

        if addr_from in pending:
            pending.remove(addr_from)
            continue
        # Jeśli nie jest to odpowiedź, na którą czekam, to daję możliwość użytkownikowi na odpowiedź
        msg = input("Odpowiedz: ")
        uniSock.sendto( msg.encode() , addr_from)
