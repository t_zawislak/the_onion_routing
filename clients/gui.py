#!/bin/usr/python3 python
from tkinter import *
import func
import sys
import time
import socket

class Application(Frame):
    def Quit(self):
        func.terminator = True
        sys.exit()
    def send_msg(self):
        # funkcja wywoływana przez naciśniecie SEND, w celu wysłania wiadomości. Brakuje error handling
        msg = self.send_txt.get()
        try:
            socket.inet_aton(self.send_ip.get())
        except:
            print("Format adresu IP jest niepoprawny")
            return
        try:
            int(self.send_port.get())
        except:
            print("Format portu jest niepoprawny")
            return
        dest = "('" + self.send_ip.get() + "', " + self.send_port.get() + ")"


        func.send( self.uniSock, msg, eval(dest) )
        time.sleep(0.05)


    def createWidgets(self):
        # address
        self.l_address = Label(self, text="Address (np. 127.0.0.1)")
        self.l_address.grid(column=0, row=0)
        self.send_ip = Entry(self, width=25)
        self.send_ip.grid(column=1, row=0)
        # port
        self.l_port = Label(self, text="port (np. 15001)")
        self.l_port.grid(column=0, row=1)
        self.send_port = Entry(self, width=25)
        self.send_port.grid(column=1, row=1)
        # wiadomosc
        self.l_msg = Label(self, text="wiadomosc: ")
        self.l_msg.grid(column=0, row=2)
        self.send_txt = Entry(self, width=25)
        self.send_txt.grid(column=1, row=2)
        # send button
        self.send_btn = Button(self, text="SEND", command=self.send_msg)
        self.send_btn.grid(column=1, row=3)
        # quit button
        self.QUIT = Button(self, text="QUIT", fg="red", command=self.Quit )
        self.QUIT.grid(column=1, row=4)

    def __init__(self, _uniSock, master=None):
        Frame.__init__(self, master)
        self.uniSock = _uniSock
        self.pack()
        self.createWidgets()
