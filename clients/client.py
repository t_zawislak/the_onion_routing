#!/usr/bin/python3 python

import socket
import struct
from threading import Thread
import func
import gui
from tkinter import *


# Utwórz nowe połączenie udp unicast
UDP_IP = socket.gethostbyname(socket.gethostname())
UDP_PORT = 15000
uniSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
uniSock.settimeout(3)

# Wybierz następny wolny port
while True:
    try:
        uniSock.bind((UDP_IP, UDP_PORT))
        print("Mój port: ", UDP_PORT)
        break
    except:         # jeśli port jest zajęty, zwiększ numer portu
        UDP_PORT+=1

print(UDP_IP)
print(UDP_PORT)
# wczytuję adres "mster node", od którego dostaję listę pozostałych nodów w sieci

func.CzytajAdress(uniSock)
func.GetInitialInodesList(uniSock)
if not func.inodes:
    print("Nie znaleziono żadnego połączenia. Proszę zmienić adres master i spróbować ponownie.")
    sys.exit(1)
print(func.inodes)



# słucha nowych wiadomości (w tym odpowiedzi na wysłane) na unicast
listen_uni = Thread( target=func.listen, args=(uniSock,))
listen_uni.start()


# start gui:
root = Tk()
root.title( "%s:%s"%(UDP_IP,str(UDP_PORT)) ) # nazywam okno swoim adresem
app = gui.Application(uniSock, master=root)
app.mainloop()
root.destroy()

# exit
isexit = input()
while(isexit != "exit"):
    isexit = input()
print("success", isexit)
func.terminator = True #zakończ wątki
time.sleep(1)
sys.exit(0)
